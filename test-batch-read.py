#!/usr/bin/env python3

"""
Example that creates a compound dataset at half precision and then
reads it in as full precision.
"""


from h5py import File
import numpy as np
from pathlib import Path

def make_file(path):
    """Make a file

    Make a file with a rank 2 dataset, compound data, half precision
    for at least one entry.

    """
    dtype = [('int', int), ('extra', float), ('half', 'f2')]
    size = 10_334
    nobj = 10
    arr = np.empty((size, nobj), dtype=dtype)
    arr['half'] = np.random.random_sample(size=(size,nobj))
    arr['int'] = np.arange(size*nobj).reshape(size,nobj)
    arr['extra'] = np.random.random_sample(size=(size,nobj))
    path.unlink(missing_ok=True)
    with File(path, 'w') as fl:
        fl.create_dataset('x', data=arr)

def as_full(typestr):
    """Convert float type to full precision

    Return an element of a dtype as a full precision float if we
    stored half.

    """
    t = np.dtype(typestr)
    if t.kind == 'f' and t.itemsize == 2:
        return np.dtype('f4')
    return t

def yield_as_full(ds, batch_size=1000, fields=[]):
    """Generator that will return batches

    Note that this will overwrite the array on each iteration, make
    sure you copy it if before the iteration ends.

    """
    keep = set(ds.dtype.names)
    if selected := set(fields):
        if missing := selected - keep:
            raise ValueError("missing fields: {}".format(', '.join(missing)))
        keep = selected
    dtype = [(n, as_full(x)) for n, x in ds.dtype.descr if n in keep]
    # start with an empty array of zero size, later we check to
    # make sure it fits the batch.
    ar = np.empty(0, dtype=dtype)
    max_size = ds.shape[0]
    for low in range(0, max_size, batch_size):
        high = min(low + batch_size, max_size)
        newshape = (high - low,) + ds.shape[1:]
        ar.resize(newshape, refcheck=False)
        ds.read_direct(ar, np.s_[low:high])
        yield ar

if __name__ == '__main__':
    path = Path('/tmp/test.h5')
    # build the file in some temporary place, note that this will
    # overwrite the last file
    make_file(path)

    # iterate through the array, casting to full precision on each read
    # you can also exclude some fields
    with File(path, 'r') as fl:
        ds = fl['x']
        for arr in yield_as_full(ds, fields={'int','half'}):
            print(arr.dtype, arr[0,0])
