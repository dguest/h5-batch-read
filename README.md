H5 full precision reader
========================

This is a simple example using a batch iterator to read out a dataset
where some elements are stored as half precision. It casts to full
precision to avoid some possible issues people saw with math on
halves.
